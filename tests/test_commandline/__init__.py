# Copyright 2017-2023 Louis Paternault
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""Tests"""

import os
import pathlib
import subprocess
import sys
import unittest

from wand.image import Image

if "COVERAGE_PROCESS_START" in os.environ:
    EXECUTABLE = ["coverage", "run"]
else:
    EXECUTABLE = [sys.executable]

TEST_DATA_DIR = pathlib.Path(__file__).parent / "data"

FIXTURES = [
    {
        "command": [
            "--algorithm",
            "panel",
            "--gap",
            ".5cm",
            "--margin",
            "1cm",
            TEST_DATA_DIR / "pcb.pdf",
        ],
        "returncode": 0,
        "env": {"PDFBACKEND": "pypdf"},
        "diff": ("pcb-nup.pdf", "pcb-control.pdf"),
    },
    {
        "command": [
            "--algorithm",
            "panel",
            "--gap",
            "1.5cm",
            "--margin",
            "3cm 1.5cm 0.5cm 2.5cm",
            TEST_DATA_DIR / "margins.pdf",
        ],
        "returncode": 0,
        "env": {"PDFBACKEND": "pypdf"},
        "diff": ("margins-nup.pdf", "margins-control.pdf"),
    },
    {
        "command": [TEST_DATA_DIR / "trigo.pdf"],
        "env": {"PDFBACKEND": "pypdf"},
        "returncode": 0,
        "diff": ("trigo-nup.pdf", "trigo-control-pypdf.pdf"),
    },
    {
        "command": [TEST_DATA_DIR / "trigo.pdf"],
        "env": {"PDFBACKEND": "pymupdf"},
        "returncode": 0,
        "diff": ("trigo-nup.pdf", "trigo-control-pymupdf.pdf"),
    },
    {
        "command": [TEST_DATA_DIR / "three-pages.pdf"],
        "env": {"PDFBACKEND": "pypdf"},
        "returncode": 0,
        "diff": ("three-pages-nup.pdf", "three-pages-control.pdf"),
    },
    {"command": [TEST_DATA_DIR / "malformed.pdf"], "returncode": 1},
    {
        "command": [TEST_DATA_DIR / "zero-pages.pdf"],
        "returncode": 1,
        "stderr": "Error: PDF files have no pages to process.\n",
    },
    {
        "command": [TEST_DATA_DIR / "dummy.pdf"],
        "env": {"PDFBACKEND": "pypdf"},
        "returncode": 1,
        "stderr": "Error: A PDF page have a null dimension.\n",
    },
    {
        "command": [TEST_DATA_DIR / "rotated.pdf"],
        "env": {"PDFBACKEND": "pypdf"},
        "returncode": 0,
        "diff": ("rotated-nup.pdf", "rotated-pypdf.pdf"),
    },
    {
        "command": [TEST_DATA_DIR / "rotated.pdf"],
        "env": {"PDFBACKEND": "pymupdf"},
        "returncode": 0,
        "diff": ("rotated-nup.pdf", "rotated-pymupdf.pdf"),
    },
    {
        "command": [TEST_DATA_DIR / "conversion.pdf"],
        "env": {"PDFBACKEND": "pypdf"},
        "returncode": 0,
        "diff": ("conversion-nup.pdf", "conversion-pypdf.pdf"),
    },
    {
        "command": [TEST_DATA_DIR / "conversion.pdf"],
        "env": {"PDFBACKEND": "pymupdf"},
        "returncode": 0,
        "diff": ("conversion-nup.pdf", "conversion-pymupdf.pdf"),
    },
]


class TestCommandLine(unittest.TestCase):
    """Run binary, and check produced files."""

    def assertPdfEqual(self, filea, fileb):
        """Test whether PDF files given in argument (as file names) are equal.

        Equal means: they look the same.
        """
        # pylint: disable=invalid-name
        images = (Image(filename=filea), Image(filename=fileb))

        # Check that files have the same number of pages
        self.assertEqual(len(images[0].sequence), len(images[1].sequence))

        # Check if pages look the same
        for pagea, pageb in zip(images[0].sequence, images[1].sequence):
            self.assertEqual(pagea.compare(pageb, metric="absolute")[1], 0)

    def test_commandline(self):
        """Test binary, from command line to produced files."""
        for data in sorted(FIXTURES, key=str):
            with self.subTest(**data):
                env = data.get("env", {})
                env.update(os.environ)
                completed = subprocess.run(
                    EXECUTABLE + ["-m", "pdfautonup"] + data["command"],
                    capture_output=True,
                    text=True,
                    env=env,
                    check=False,
                )

                for key in ["returncode", "stderr", "stdout"]:
                    if key in data:
                        self.assertEqual(getattr(completed, key), data.get(key))

                if "diff" in data:
                    self.assertPdfEqual(
                        *(TEST_DATA_DIR / filename for filename in data["diff"])
                    )
