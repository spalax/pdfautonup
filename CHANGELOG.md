* pdfautonup 1.12.0 (unreleased)

    * Renamed `fitz` python dependency to `pymupdf` (same library, new name).

    -- Louis Paternault <spalax@gresille.org>

* pdfautonup 1.11.0 (2024-12-27)

    * Add Python3.13 support.

    -- Louis Paternault <spalax@gresille.org>

* pdfautonup 1.10.0 (2024-08-27)

    * It is now possible to specify different margins for each side of the paper (thanks Milo Mirate).
    * pymupdf and pypdf backends both use the same coordinate system (where (0, 0) is the bottom left corner)
    * Add python3.13 support.

    -- Louis Paternault <spalax@gresille.org>

* pdfautonup 1.9.0 (2024-02-19)

    * Add a documented `pdfautonup` function.
    * Drop python3.8 and python3.9 support.
    * Pdf backend is loaded at execution time, not at import time

    -- Louis Paternault <spalax@gresille.org>

* pdfautonup 1.8.1 (2023-11-11)

    * Fix bug: pypdf backend would fail with some files due to unit conversion.

    -- Louis Paternault <spalax@gresille.org>

* pdfautonup 1.8.0 (2023-11-09)

    * Add python3.12 support.
    * Drop python3.7 support.
    * Fix bug: PDF files with rotated pages are correctly handled (closes #22).
    * Update minimum PyMuPDF version.

    -- Louis Paternault <spalax@gresille.org>

* pdfautonup 1.7.0 (2023-02-18)

    * Update PyPDF dependency, from pypdf2 to pypdf>=3.4.0.
    * PDFBACKEND=pypdf2 is deprecated: use PDFBACKEND=pypdf instead.

    -- Louis Paternault <spalax@gresille.org>

* pdfautonup 1.6.1 (2023-01-27)

    * Fix licence: pdfautonup was still marked as licenced as GPL (instead of AGPL) in some files.
    * Fix PyPDF2 version: pdfautonup does not work (yet) with pypdf2 version 3 or later.

    -- Louis Paternault <spalax@gresille.org>

* pdfautonup 1.6.0 (2022-11-20)

    * Support for PyMuPDF>=1.21.
    * Python3.11 support.
    * Add a --verbose option.

    -- Louis Paternault <spalax@gresille.org>

* pdfautonup 1.5.1 (2022-02-15)

    * Fix compatibility issue with papersize dependency.

    -- Louis Paternault <spalax@gresille.org>

* pdfautonup 1.5.0 (2021-11-14)

    * Support for PyMuPDF>=1.19 (closes #18).
    * Fix bug which could happen with document with very very different page sizes (closes #17).
    * Change licence from GPL3+ to AGPL3+, to comply with PyMuPDF licence (closes #19).

    -- Louis Paternault <spalax@gresille.org>

* pdfautonup 1.4.0 (2021-09-05)

    * Fix and improve help message about units and paper sizes.
    * Python3.10 support.

    -- Louis Paternault <spalax@gresille.org>

* pdfautonup 1.3.0 (2020-09-07)

    * Python support:
      * Drop python3.5 support.
      * Add python3.9 support.
    * Fix bug: PyPDF2 backend would crash with a valid pdf file without any trailing info (thanks Alex Dong).
    * Fix typo in documentation.

    -- Louis Paternault <spalax@gresille.org>

* pdfautonup 1.2.0 (2019-09-07)

    * Python support
      * Drop python3.6 support.
      * Add python3.8 support
    * It is now possible to choose the Python library used to read and write
      PDF files (closes #14).

    -- Louis Paternault <spalax@gresille.org>

* pdfautonup 1.1.0 (2019-03-08)

    * Python support
      * Drop python3.4 support.
      * Python3.7 support
    * Dependencies
      * Replace PyPDF2 dependency with PyMuPDF.
    * Features and Bugs
      * Pdfautonup is faster (closes #9).
      * Can read and write from standard input and input (closes #12).
      * Pages are cropped before being merged (closes #13).

    -- Louis Paternault <spalax@gresille.org>

* pdfautonup 1.0.0 (2017-12-06)

    * Log a warning if pages have different sizes.

    -- Louis Paternault <spalax@gresille.org>

* pdfautonup 0.4.3 (2017-03-11)

    * No longer crash if PDF files have no pages (closes #10).
    * No longer crash if PDF pages have null dimensions, for instance "0cmx1cm" (closes #11).
    * Add python3.6 support.
    * Add regression tests.
    * Minor internal improvements.

    -- Louis Paternault <spalax@gresille.org>

* pdfautonup 0.4.2 (2016-10-14)

    * Move help about paper sizes into a separate "--help-paper" option.

    -- Louis Paternault <spalax@gresille.org>

* pdfautonup 0.4.1 (2016-05-21)

    * Fix error in changelog.

    -- Louis Paternault <spalax@gresille.org>

* pdfautonup 0.4.0 (2016-05-21)

    * Raise error instead of producing a PDF with no pages (closes #7).
    * Add option `--progress` (closes #8).

    -- Louis Paternault <spalax@gresille.org>

* pdfautonup 0.3.0 (2016-02-18)

    * New algorithm to arrange source documents onto destination document (from a request to panelize printed circuit boards).
    * Adding other algorithms will be easier now.
    * Add option `--orientation`, to force destination page orientation.
    * Fix centering and margin problems with the `fuzzy` (old) algorithm.
    * Minor documentation improvements

    -- Louis Paternault <spalax@gresille.org>

* pdfautonup 0.2.0 (2016-01-25)

    * Can be called as a python module : `python3 -m pdfautonup FOO` is equivalent to `pdfautonup FOO`.
    * Fix error when `paperconf` is not installed (closes #3).
    * Add `.pdf` to argument if missing (closes #4).

    -- Louis Paternault <spalax@gresille.org>

* pdfautonup 0.1.1 (2015-06-13)

    * Python3.5 support
    * In case of unreadable file (corrupted PDF, not a PDF, no-existant, etc.),
      die nicely (instead of displaying the exception stack).
    * Added project URL to generated pdf metadata
    * Several minor improvements to setup, test and documentation.

    -- Louis Paternault <spalax@gresille.org>

* pdfautonup 0.1.0 (2015-03-20)

    * First published version.

    -- Louis Paternault <spalax@gresille.org>
